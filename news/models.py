# -*- coding: utf-8 -*-

from django.db import models
from django.core.exceptions import ValidationError


class Source(models.Model):
    title = models.TextField(max_length=500)
    domain = models.URLField()

    def __unicode__(self):
        return self.title


class ParseRule(models.Model):
    name = models.CharField(max_length=200)
    source = models.ForeignKey(Source)
    url_pattern = models.CharField(max_length=500)
    content_xpath = models.CharField(max_length=200)
    title_xpath = models.CharField(max_length=200)
    author_xpath = models.CharField(max_length=200, null=True, blank=True)
    date_xpath = models.CharField(max_length=200, null=True, blank=True)
    date_format = models.CharField(max_length=200, null=True, blank=True)

    def clean(self):
        if self.date_xpath and not self.date_format:
            raise ValidationError("date_xpath and date_format is required together")

    def __unicode__(self):
        return self.name


class CommentParseRule(models.Model):
    METHOD_CHOICES = (
        (0, 'GET'),
        (1, 'POST'),
        (2, 'PUT'),
    )
    RESPONSE_CHOICES = (
        (0, 'HTML'),
        (1, 'JSON'),
        (2, 'XML'),
    )

    name = models.CharField(max_length=200)
    source = models.ForeignKey(Source)
    action = models.URLField(null=True, blank=True)
    method = models.PositiveSmallIntegerField(default=0, choices=METHOD_CHOICES)
    response_type = models.PositiveSmallIntegerField(default=0, choices=RESPONSE_CHOICES)
    block_path = models.CharField(max_length=200)
    body_path = models.CharField(max_length=200)
    author_path = models.CharField(max_length=200, null=True, blank=True)
    date_path = models.CharField(max_length=200, null=True, blank=True)
    date_format = models.CharField(max_length=200, null=True, blank=True)
    click_path = models.CharField(max_length=200, null=True, blank=True)
    paginator_path = models.CharField(max_length=200, null=True, blank=True)
    js_scpipt = models.TextField(null=True, blank=True)


    def clean(self):
        if self.action and not self.method and not self.response_type:
            raise ValidationError(u'Если вы заполнили поле action, то должны выбрать поля method, response_type ')

    def __unicode__(self):
        return self.name



class CommonModel(models.Model):
    author = models.CharField(max_length=50, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    text = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True


class News(CommonModel):
    title = models.CharField(max_length=500, null=True, blank=True)
    url = models.URLField()
    source = models.ForeignKey(Source)
    rule = models.ForeignKey(ParseRule)
    comments_rule = models.ForeignKey(CommentParseRule, null=True, blank=True)
    comments_count = models.PositiveIntegerField(default=0)
    error_log = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.title

    def comments_count_incr(self):
        self.comments_count = models.F('comments_count') + 1
        self.save()

class NewsComment(CommonModel):
    news = models.ForeignKey(News)

    def __unicode__(self):
        return self.news.title