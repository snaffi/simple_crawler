from django.contrib import admin

from news.models import Source, ParseRule, News, NewsComment, CommentParseRule
from news.tasks import crawl_resource


class ParseRuleInline(admin.StackedInline):
    model = ParseRule
    extra = 1
    fields = (
        ('name',),
        ('url_pattern', 'content_xpath', 'title_xpath'),
        ('author_xpath',),
        ('date_xpath', 'date_format'),
    )


class CommentsParseRuleInline(admin.StackedInline):
    model = CommentParseRule
    extra = 1
    fields = (
        ('name', 'source'),
        ('action', 'method', 'response_type'),
        ('block_path', 'body_path'),
        ('author_path',),
        ('date_path', 'date_format'),
        ('click_path',),
        ('paginator_path',),
        ('js_scpipt',),
    )


class SourceAdmin(admin.ModelAdmin):
    inlines = [
        ParseRuleInline,
        CommentsParseRuleInline,
    ]


class NewsCommentAdmin(admin.StackedInline):
    model = NewsComment
    extra = 0


class NewsAdmin(admin.ModelAdmin):
    inlines = [
        NewsCommentAdmin,
    ]

    def save_model(self, request, obj, form, change):
        obj.save()
        crawl_resource.delay(obj.pk)


admin.site.register(Source, SourceAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(NewsComment)