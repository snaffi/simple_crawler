# -*- coding: utf-8 -*-
from __future__ import absolute_import

import time
import requests
import datetime

from lxml import html
from celery import shared_task
from news.models import News, NewsComment

from selenium import webdriver

@shared_task
def crawl_resource(pk):
    news = News.objects.filter(pk=pk).first()
    if news:
        page = requests.get(news.url)
        tree = html.fromstring(page.text)
        news.text = ''.join(tree.xpath(news.rule.content_xpath))
        news.title = ''.join(tree.xpath(news.rule.title_xpath))
        if news.rule.date_xpath:
            date_str = tree.xpath(news.rule.date_xpath)
            if date_str:
                date = datetime.datetime.strptime(date_str[0], news.rule.date_format)
                news.date = date
        if news.rule.author_xpath:
            author = ', '.join(tree.xpath(news.rule.author_xpath))
            if author:
                news.author = author
        news.save()
        crawl_resource_comments.delay(news.pk)

@shared_task
def crawl_resource_comments(pk):
    news = News.objects.filter(pk=pk).first()
    if news and news.comments_rule:
        try:
            driver = webdriver.PhantomJS()
            print("Driver created")
            driver.set_window_size(1120, 550)
            print("Driver init")
            driver.get(news.url)
            print("page geted")
            if news.comments_rule.click_path:
                click_element = driver.find_element_by_xpath(news.comments_rule.click_path)
                if click_element:
                    time.sleep(5)
                    click_element.click()
            if news.comments_rule.paginator_path:
                print("=============================> ", news.comments_rule.paginator_path)
                paginator = driver.find_elements_by_xpath(news.comments_rule.paginator_path)
                print("PAGINATOR", paginator)
                for n, elem in enumerate(paginator):
                    driver.find_elements_by_xpath(news.comments_rule.paginator_path)[n].click()
                    time.sleep(5)
                    print("=============================>    parse_comments")
                    parse_comments(driver.page_source, news)
            else:
                print("=============================>    parse_comments")
                parse_comments(driver.page_source, news)
            driver.quit()
        except Exception, e:
            news.error_log = str(e)
            news.save()
        finally:
            driver.quit()


def parse_comments(source, news):
    tree = html.fromstring(source)
    for comment in tree.xpath(news.comments_rule.block_path):
        news_comment = NewsComment()
        news_comment.news = news
        news_comment.text = ''.join(comment.xpath(news.comments_rule.body_path))
        if news.comments_rule.date_path:
            date_str = tree.xpath(news.comments_rule.date_path)
            if date_str:
                date = datetime.datetime.strptime(date_str[0], news.rule.date_format)
                news_comment.date = date
        if news.comments_rule.author_path:
            author = ' '.join(comment.xpath(news.comments_rule.author_path))
            if author:
                news_comment.author = author
        news_comment.save()
        news.comments_count_incr()