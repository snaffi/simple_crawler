#!/bin/bash
sudo apt-get update
sudo apt-get install python-dev python-virtualenv libffi-dev libxml2-dev libssl-dev libxslt-dev python-pip libpq-dev  docker.io phantomjs -y
sudo apt-get install build-essential
sudo apt-get install tcl8.5
sudo apt-get install docker.io

wget http://download.redis.io/releases/redis-2.8.9.tar.gz
tar xzf redis-2.8.9.tar.gz
cd redis-2.8.9
make
sudo make install
cd utils
sudo ./install_server.sh
sudo service redis_6379 start
sudo update-rc.d redis_6379 defaults


cd /vagrant
virtualenv -p python .env
sudo /vagrant/.env/bin/pip install -r requirements.txt